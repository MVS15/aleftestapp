import React, {useState, useEffect} from 'react';
import './App.css';
import Home from './pages/Home';
import Preview from './pages/Preview';
import {Route, Routes, Link, useLocation} from 'react-router-dom';

function App() {
	const location = useLocation();
	
	const aboutMeInitVals = {name : '', age : ''}
	const aboutChildsInitVals = {name : '', age : ''}
	
	const [infoFormData, setInfoFormData] = useState({
		aboutMe : {...aboutMeInitVals},
		aboutChilds : [],
		readyToSend: false
	});
	
	const [infoPreviewData, setInfoPreviewData] = useState({});
	
	return (
		<div className="App">
			<header>
				<div className="logo">
					<img className="logo__img" src="/logo.png" />
				</div>
				
				<div className="nav">
					<Link 
						className={"nav__item link link--simple " + (location.pathname === '/' ? 'link--active' : '')} 
						to="/"
					>Форма</Link>
					
					<Link 
						className={"nav__item link link--simple " + (location.pathname === '/preview' ? 'link--active' : '')} 
						to="/preview"
					>Превью</Link>
				</div> 
			</header>
			
			<main>
				<Routes>
					<Route path="/" element={<Home 
						infoFormData={infoFormData} 
						setInfoFormData={setInfoFormData}  
						setInfoPreviewData={setInfoPreviewData} 
						aboutChildsInitVals={aboutChildsInitVals} 
					/>} />
					
					<Route path="/preview" element={<Preview 
						infoPreviewData={infoPreviewData} 
					/>} />
				</Routes>
			</main>
			
			<footer>
				All rights reserved
			</footer>
		</div>
	);
}

export default App;
