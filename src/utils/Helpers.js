export const isEmpty = (value) => {
	if(typeof value === 'string'){
		value = value.trim();
		
		if(value === '') return true;
	}
	
	if(typeof value === 'undefined' || value === undefined){
		return true;
	}
	
	if(value === null) return true; 
	
	return false
}

export const getNumWord = (num, words) => {  
	num = Math.abs(num); 
	
	const mod10 = num % 10;
	const mod100 = num % 100;
	
	if(mod100 > 10 && mod100 < 20){
		return words[2];
	}
	
	if(mod10 > 1 && mod10 < 5){
		return words[1];
	}
	
	if(mod10 == 1) return words[0];
	
	return words[2];
}