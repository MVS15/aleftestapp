import React from 'react';
import {getNumWord, isEmpty} from '../utils/Helpers';
import '../pages/Preview.css';

function Preview({infoPreviewData}) {
	const yearWords = ['год', 'года', 'лет'];
	
	return (
		<div className="info-preview">
			<div className="info-preview__item info-preview__item--about-me">
				<h2 className="info-preview__item-title">Персональные данные</h2>
			
				{infoPreviewData.aboutMe
					? <div className="info-preview__results">
						<div className="info-preview__result">
							{infoPreviewData.aboutMe.name}, {infoPreviewData.aboutMe.age} {getNumWord(infoPreviewData.aboutMe.age, yearWords)}
						</div>
					</div>
					: <div className="info-preview__no-results">Нет данных</div>
				}
			</div>
			
			<div className="info-preview__item info-preview__item--about-childs">
				<h2 className="info-preview__item-title">Дети</h2>
				
				{(infoPreviewData.aboutChilds && infoPreviewData.aboutChilds.length)
					? <div className="info-preview__results">
						{infoPreviewData.aboutChilds.map((child, index) => 
							<div key={index} className="info-preview__result">
								{child.name}, {child.age} {getNumWord(child.age, yearWords)}
							</div>	
						)}
					</div>
					: <div className="info-preview__no-results">Нет данных</div>
				}
			</div>
		</div>
	);
}

export default Preview;
