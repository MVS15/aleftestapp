import React, {useState} from 'react';
import {isEmpty} from '../utils/Helpers';
import {useNavigate} from 'react-router-dom';
import '../pages/Home.css';

function Home({infoFormData, setInfoFormData, setInfoPreviewData, aboutChildsInitVals}) {
	const navigate = useNavigate();
	
	const updateInfoFormData = (operation, info, data) => {
		const newInfoFormData = {
			aboutMe : {...infoFormData.aboutMe},
			aboutChilds : infoFormData.aboutChilds.map(child => {
				return {...child}
			}),
			readyToSend : false
		};
		
		if(data && data.prop){
			if(data.prop == 'age'){
				data.value = data.value.replace(/\D+/, '');
			}else if(data.prop == 'name'){
				data.value = data.value.replace(/[^а-яА-Яa-zA-Z0-9\s]+/, '');
			}
		}
		
		if(operation == 'update'){
			if(info == 'aboutMe'){
				newInfoFormData[info][data.prop] = data.value;
			}else if(info == 'aboutChilds'){
				newInfoFormData[info][data.index][data.prop] = data.value;
			}
		}else if(operation == 'insert'){
			if(info == 'aboutChilds'){
				newInfoFormData[info].push({...aboutChildsInitVals});
			}
		}else if(operation == 'delete'){
			if(info == 'aboutChilds'){
				newInfoFormData[info] = newInfoFormData[info].filter((child, index) => index != data.index);
			}
		}
		
		newInfoFormData['readyToSend'] = true;
		
		if(isEmpty(newInfoFormData['aboutMe'].name) || isEmpty(newInfoFormData['aboutMe'].age)){
			newInfoFormData['readyToSend'] = false
		}
		
		if(newInfoFormData['aboutChilds'].length){
			newInfoFormData['aboutChilds'].map(child => {
				if(isEmpty(child.name) || isEmpty(child.age)){
					newInfoFormData['readyToSend'] = false;
				}
			});
		}
		
		setInfoFormData(newInfoFormData);
	}
	
	const saveInfoFormData = () => {
		if(infoFormData.readyToSend){
			const newInfoPreviewData = {
				aboutMe : {...infoFormData.aboutMe},
				aboutChilds : infoFormData.aboutChilds.map(child => {
					return {...child}
				}),
			};
			
			setInfoPreviewData(newInfoPreviewData);
			
			navigate('/preview')
		}
	}
	
	return (
		<form className="info-form">
			<div className="info-form__item info-form__item--about-me">
				<h2 className="info-form__item-title">Персональные данные</h2>
				
				<div className="info-form__fields info-form__fields--vertical">
					<div className="info-form__field">
						<input 
							className="input info-form__input" 
							type="text" 
							placeholder="Имя" 
							value={infoFormData.aboutMe.name}
							onChange={(e) => {
								updateInfoFormData('update', 'aboutMe', {prop : 'name', value : e.target.value});
							}}
						/>
					</div>
				
					<div className="info-form__field">
						<input 
							className="input info-form__input" 
							type="text" 
							placeholder="Возраст" 
							value={infoFormData.aboutMe.age} 
							onChange={(e) => {
								updateInfoFormData('update', 'aboutMe', {prop : 'age', value : e.target.value});
							}}
						/>
					</div>
				</div>
			</div>
			
			<div className="info-form__item info-form__item--about-childs">
				<h2 className="info-form__item-title">
					Дети(макс. 5)
					
					{infoFormData.aboutChilds.length < 5
						? <button 
							className="btn btn--secondary info-form__new-child-btn" 
							type="button"
							onClick={(e) => {
								updateInfoFormData('insert', 'aboutChilds');
							}}
						>+ Добавить ребенка</button>
						: ''
					}
				</h2>
				
				{infoFormData.aboutChilds.map((child, index) => 
					<div key={index} className="info-form__fields info-form__fields--horizontal">
						<div className="info-form__field">
							<input 
								className="input info-form__input" 
								type="text" 
								placeholder="Имя"
								value={infoFormData.aboutChilds[index].name}
								onChange={(e) => {
									updateInfoFormData('update', 'aboutChilds', {prop : 'name', value : e.target.value, index : index});
								}}
							/>
						</div>
						
						<div className="info-form__field">
							<input 
								className="input info-form__input" 
								type="text" 
								placeholder="Возраст" 
								value={infoFormData.aboutChilds[index].age}
								onChange={(e) => {
									updateInfoFormData('update', 'aboutChilds', {prop : 'age', value : e.target.value, index : index});
								}}
							/>
						</div>
						
						<button 
							className="btn btn--simple" 
							type="button"
							onClick={(e) => {
								updateInfoFormData('delete', 'aboutChilds', {index : index});
							}}
						>Удалить</button>
					</div>
				)}
			</div>
			
			<div className="info-form__item">
				<button 
					disabled={!infoFormData.readyToSend} 
					className="btn btn--primary" 
					type="button"
					onClick={saveInfoFormData}
				>Сохранить</button>
			</div>
		</form>
	);
}

export default Home;
